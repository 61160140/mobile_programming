//
//  CourseView.swift
//  Homework61160140
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct CourseView: View {
    var body: some View {
        List(0 ..< 5) { item in
            Apple()
        }
    }
}

struct CourseView_Previews: PreviewProvider {
    static var previews: some View {
        CourseView()
    }
}
