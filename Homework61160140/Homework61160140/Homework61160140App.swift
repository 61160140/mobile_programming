//
//  Homework61160140App.swift
//  Homework61160140
//
//  Created by student on 12/18/20.
//

import SwiftUI

@main
struct Homework61160140App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
