//
//  Apple.swift
//  Homework61160140
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct Apple: View {
    var body: some View {
        HStack(alignment: .top){
            Image(systemName: "applelogo").resizable().frame(width: 25.0, height: 30.0)
                
            VStack(alignment: .leading){
                HStack{
                    Text("Apple")
                    Image(systemName: "checkmark.seal.fill").renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    Text("@Apple").foregroundColor(Color.secondary)
                }
                Text("นำ iPhone ของคุณมาแลก แล้วอัพเกรดเป็น iPhone 12 mini ใหม่")
                Image("apple").resizable().frame(width: 250.0, height: 250.0)

                Text("ขอแนะนำ iPhone 12 mini")
                Text("apple.com").font(.footnote).foregroundColor(Color.secondary)
                HStack{
                    Image(systemName: "message").resizable()
                        .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(.gray)
                        .frame(width: 15.0, height: 15.0)
                    Text("1").foregroundColor(.gray).multilineTextAlignment(.leading).padding(.trailing, 25.0)
                    
                    Image(systemName: "arrow.2.squarepath").resizable()
                        .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(.gray)
                        .frame(width: 17.0, height: 15.0)
                    Text("11").foregroundColor(.gray).padding(.trailing, 25.0)
                    
                    Image(systemName: "heart").resizable()
                        .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(.gray)
                        .frame(width: 15.0, height: 15.0)
                    Text("28").foregroundColor(.gray).padding(.trailing, 25.0)
                    
                    Image(systemName: "square.and.arrow.up").resizable()
                        .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(.gray)
                        .frame(width: 15.0, height: 15.0)
                }
                .padding(.top)
                HStack{
                    Image(systemName: "arrow.up.forward.app.fill").resizable()
                        .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/).foregroundColor(.gray)
                        .frame(width: 15.0, height: 15.0)
                    Text("Promoted").font(.footnote).foregroundColor(Color.secondary)
                }
            }
        }
        .padding()
    }
}

struct Apple_Previews: PreviewProvider {
    static var previews: some View {
        Apple()
    }
}
