//
//  ContentView.swift
//  Test
//
//  Created by student on 12/4/20.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        SideBar()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
