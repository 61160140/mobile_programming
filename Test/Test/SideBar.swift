//
//  SideBar.swift
//  Test
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct SideBar: View {
    var body: some View {
        NavigationView {
        content
            .navigationTitle("Learn")
            .toolbar{
                ToolbarItem(placement: .navigationBarTrailing){
                    HStack{
                        Image(systemName: "person.crop.circle")
                        Text("Login")
                    }
                }
            }
            
            CoursesView()
        }
    }
        
        var content: some View {
            List{
                NavigationLink(destination: CoursesView()){
                    Label("Course", systemImage: "book.closed")
                }
                Label("Tutorial", systemImage: "list.bullet.rectangle")
                Label("Livestreams", systemImage: "tv")
                Label("Certificates", systemImage: "mail.stack")
                Label("Search", systemImage: "magnifyingglass")
            }
            .listStyle(SidebarListStyle())
        }
}

struct SideBar_Previews: PreviewProvider {
    static var previews: some View {
        SideBar()
    }
}
