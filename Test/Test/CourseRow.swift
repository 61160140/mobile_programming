//
//  CourseRow.swift
//  Test
//
//  Created by student on 12/18/20.
//

import SwiftUI

struct CourseRow: View {
    var body: some View {
        HStack(alignment: .top){
            Image(systemName: "paperplane.circle.fill")
                .renderingMode(/*@START_MENU_TOKEN@*/.template/*@END_MENU_TOKEN@*/)
                .frame(width: 48.0, height: 48.0)
                .imageScale(.large)
                .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.yellow/*@END_MENU_TOKEN@*/)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
            VStack(alignment: .leading, spacing: 4.0){
                Text("SwiftUI").font(.subheadline).fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                Text("Description").font(.footnote)
                    .foregroundColor(Color.secondary)
            }
            Spacer()
        }
        //.font(.system(size: 34, weight: .light, design: .rounded))
    }
}

struct CourseRow_Previews: PreviewProvider {
    static var previews: some View {
        CourseRow()
    }
}
