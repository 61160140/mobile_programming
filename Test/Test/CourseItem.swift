//
//  CourseItem.swift
//  Test
//
//  Created by student on 12/25/20.
//

import SwiftUI

struct CourseItem: View {
    var body: some View {
        VStack(alignment: .trailing, spacing: 4.0) {
            Spacer()
            Image("Illustration 1")
                .resizable()
                .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fit/*@END_MENU_TOKEN@*/)
            Text("Mobile Programming")
                .fontWeight(.bold)
                .foregroundColor(Color.pink)
                
            Text("Section1")
                .font(.footnote)
                .fontWeight(.regular)
                .foregroundColor(Color.blue)
        }
        .padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
        .background(/*@START_MENU_TOKEN@*//*@PLACEHOLDER=View@*/Color.yellow/*@END_MENU_TOKEN@*/)
        .cornerRadius(/*@START_MENU_TOKEN@*/20.0/*@END_MENU_TOKEN@*/)
        .shadow(radius: /*@START_MENU_TOKEN@*/20/*@END_MENU_TOKEN@*/)
    }
}

struct CourseItem_Previews: PreviewProvider {
    static var previews: some View {
        CourseItem()
    }
}
