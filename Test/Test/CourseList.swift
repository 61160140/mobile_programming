//
//  CourseList.swift
//  Test
//
//  Created by student on 12/25/20.
//

import SwiftUI

struct CourseList: View {
    @State var show = false
    @Namespace var namespace
    var body: some View {
        ZStack{
            ScrollView{
                VStack(spacing: 20.0){
                    CourseItem()
                        .matchedGeometryEffect(id: "Card", in: namespace, isSource: !show)
                        .frame(width: 335, height: 250)
                    CourseItem()
                        .frame(width: 335, height: 250)
                }
                .frame(maxWidth: .infinity)
            }
            VStack{
                if show {
                    ScrollView{
                        CourseItem()
                            .matchedGeometryEffect(id: "Card", in: namespace)
                            .frame(height: 345)
                        ForEach(0..<20) { item in
                            CourseRow()
                                .padding(.bottom, 5.0)
                        }
                    }
                    .background(Color("Background 1"))
                    //.transition(AnyTransition
                    //              .opacity
                    //            .animation(Animation.spring().delay(0.3)))
                    .transition(.asymmetric(insertion: AnyTransition
                                                .opacity
                                                .animation(Animation.spring().delay(0.3))
                                            , removal: AnyTransition
                                                .opacity
                                                .animation(Animation.spring().delay(0.3))
                    ))
                    .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                    
                }
            }
        }
        .onTapGesture {
            withAnimation(.spring()){
                show.toggle()
            }
        }
    }
}

struct CourseList_Previews: PreviewProvider {
    static var previews: some View {
        CourseList()
    }
}
